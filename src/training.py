#!  /usr/bin/python

from nltk.corpus import gutenberg
from nltk.corpus import webtext
from nltk.corpus import brown
from nltk.corpus import reuters
from nltk.corpus import inaugural
from nltk.corpus import treebank
from nltk.corpus import names
from nltk.corpus import abc 
from nltk.corpus import genesis 
from nltk.corpus import state_union 
from nltk.corpus import switchboard , ycoe
from nltk.corpus import semcor, ieer, ptb, sinica_treebank
from nltk.corpus import names, stopwords, words
from nltk.corpus import conll2002, conll2007, conll2000

from nltk.tag import pos_tag

import sys
import perceplearn

# Description : main function which reads data from nltk corpus, transforms it into the right format, trains the model using perceplearn and calls all the necessary functions 
# function parameters:
# none 
# return value:
# none 
def main():
	count = {}
	sentbank = []
	inputDict = []
	openAndRead(gutenberg, count, sentbank)
	openAndRead(webtext, count, sentbank)
	openAndRead(brown, count, sentbank)
	openAndRead(reuters, count, sentbank)
	openAndRead(inaugural, count, sentbank)
	openAndRead(genesis, count, sentbank)
	openAndRead(state_union, count, sentbank)

	inputDict.append([]) 
	inputDict.append([]) 
	inputDict.append([]) 
	inputDict.append([]) 
	inputDict.append([]) 

	for key, value in count.items():
		dindex = finddoc(key)
		flag = 0
		for v in sentbank[value]:
			x = pos_tag(v)
			ind, flag = findindex(v, key)
			
			prev2L = x[ind - 2][1] if ind > 1 else " " 
			prevL = x[ind - 1][1] if ind > 0 else " " 
			nextL = x[ind + 1 + flag][1] if (ind + flag) < (len(x) - 1) else " "	
			next2L = x[ind + 2 + flag][1] if (ind + flag) < (len(x) - 2) else " "	
			prev2W = x[ind - 2][0] if ind > 1 else " " 
			prevW = x[ind - 1][0] if ind > 0 else " " 
			nextW = x[ind + 1 + flag][0] if (ind + flag) < (len(x) - 1) else " "
			next2W = x[ind + 2 + flag][0] if (ind + flag) < (len(x) - 2) else " "
			inputDict[dindex].append(key + " prevLabel::" + prevL + " nextLabel::" + nextL + " prevWord::" + prevW + " nextWord::" + nextW + " prev2Label::" + prev2L + " next2Label::" + next2L + " prev2Word::" + prev2W + " next2Word::" + next2W)  
			
	for ii in range(0, 5):
		model = open(sys.argv[1] + "-" + str(ii), 'w')
		total= len(inputDict[ii])
		dict1 = {}
		w_avg = [dict() for x in range(0)]
		perceplearn.mainlearn(dict1, w_avg, inputDict[ii], total)

		for kd, vd in dict1.items():
			if kd == '' or kd == ' ':
				kd = 'SPACE'
			model.write(kd + ' ' + str(vd) + ' ')
			for kw, vw in w_avg[vd].items():
				kw = kw.replace('\n', '')
				model.write(kw + ' ' + str(vw) + ' ') 
			model.write('\n')

		model.close()

# Description : index of the word belonging to class key 
# function parameters:
# v - sentence
# key - class  
# return value:
# base index and offset 
def findindex(v, key):
	flag = 0
	if key == 'it_s':
		key = 'it'
		flag = 2
	if key == 'you_re':
		key = 'you'
		flag = 2
	if key == 'they_re':
		key = 'they'
		flag = 2
	
	for i in range(0, len(v)):
		if key == v[i]:
			break 
	return i, flag 

# Description : class index 
# function parameters:
# key - class  
# return value:
# class index 
def finddoc(key):
	if key == 'its' or key == 'it_s':
		return 0
	elif key == 'your' or key == 'you_re':
		return 1	
	elif key == 'they_re' or key == 'their':
		return 2 
	elif key == 'lose' or key == 'loose':
		return 3	
	elif key == 'to' or key == 'too':
		return 4	

# Description : read from corpus files 
# function parameters:
# corpusID 
# count - dictionary to be populated 
# sentbank - sentence bank 
# return value:
# none 
def openAndRead(corpusID, count, sentbank):
	flag = 0 
	for file in corpusID.fileids():
		print(file)
		num_sent = len(corpusID.sents(file))
		sents = corpusID.sents(file)
		for sent in sents:
			for word in sent:
				flag = count1(word, sent, count, flag, sentbank)
		#break

# Description : update sentence bank 
# function parameters:
# key 
# count - dictionary to be populated 
# sent - sentence 
# sentbank - sentence bank 
# return value:
# none 
def updatesentbank(key, count, sent, sentbank):
	if key not in count:
		count[key] = len(count)
		sentbank.append([])
		sentbank[count[key]].append(sent)
	sentbank[count[key]].append(sent)


# Description : update count and sentence bank 
# function parameters:
# word - dictionary to be populated 
# sent - sentence 
# count - dictionary to be populated 
# flag 
# sentbank - sentence bank 
# return value:
# flag 
def count1(word, sent, count, flag, sentbank):
	if 'its' == word.lower():
		updatesentbank("its", count, sent, sentbank)
	if 'your' == word.lower():
		updatesentbank("your", count, sent, sentbank)
	if 'their' == word.lower():
		updatesentbank("their", count, sent, sentbank)
	if 'lose' == word.lower():
		updatesentbank("lose", count, sent, sentbank)
	if 'too' == word.lower():
		updatesentbank("too", count, sent, sentbank)
	if "your're" == word.lower():
		updatesentbank("your_re", count, sent, sentbank)
	if 'loose' == word.lower():
		updatesentbank("loose", count, sent, sentbank)
	if 'to' == word.lower():
		updatesentbank("to", count, sent, sentbank)
	if flag == 2:
		if "s" == word.lower(): 
			updatesentbank("it_s", count, sent, sentbank)
		flag = 0 
	if flag == 1: 
		if "'" == word: 
			flag = 2 
		else:
			flag = 0
	if "it" == word.lower():
		flag = 1 
	if flag == 4:
		if "re" == word.lower(): 
			updatesentbank("you_re", count, sent, sentbank)
		flag = 0 
	if flag == 3:
		if "'" == word: 
			flag = 4 
		else:
			flag = 0
	if "you" == word.lower():
		flag = 3 
	if flag == 6:
		if "re" == word.lower(): 
			updatesentbank("they_re", count, sent, sentbank)
		flag = 0 
	if flag == 5:
		if "'" == word: 
			flag = 6 
		else:
			flag = 0
	if "they" == word.lower():
		flag = 5 
	return flag

if __name__ == "__main__":
	main()
