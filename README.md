#HW3 Report


#Data Source: 
- nltk corpus

#Third Part tools: 
- nltk - pos tagging

#Approach

A number of documents from nltk corpus are scanned and the sentences containing the homonyms of interest are added to lists. In total there are 5 lists, containing training examples of two classes.

- 0 : its / it's
- 1 : your / you're
- 2 : they're / their
- 3 : lose / loose
- 4 : to / too

The training data is tagged using nltk pos tagging.

The data is converted to the form :

<class label> prevLabel::<prevL> nextLabel::<nextL> prevWord::<prevW> nextWord::<nextW> prev2Label::<prev2L> next2Label::<next2L> prev2Word::<prev2W> next2Word::<next2W>

So, we have 5 binary classification problems. Perceptron designed in HW2 is used to solve this.
Then we pass the data to the perceptron learn, to train the model. We'll have 5 model files.


Prediction : 

The test file is also scanned for the relevant homonyms sentence-wise. If a sentence contains one or more homonyms, test data is constructed from that word of the same format as training data.

<current class label> prevLabel::<prevL> nextLabel::<nextL> prevWord::<prevW> nextWord::<nextW> prev2Label::<prev2L> next2Label::<next2L> prev2Word::<prev2W> next2Word::<next2W>

This is passed to the perceptron classifier, which returns the predicted label. The data is written to an output file maintaining the formatting, changing the original label to the predicted label.

src files:

#training.py 
- generate model files for correcting errors in documents related to relevant homonyms

Syntax :	python3 training.py <model-file name>
	
#predict.py 
- correct the errors in test file using the model files generated during training

Syntax :	python3 predict.py <input file> <model-file name> <output file>

#perceplearn.py 
- perceptron training(called from training.py)

#percepclassify.py 
- perceptron classify(called from predict.py)


#Accuracy

- 0.9546100442531769 (on dev data)
