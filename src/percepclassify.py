#! /usr/bin/python

import fileinput
import sys 
import codecs 

def prediction(dict1, weight, fx):
	maxV = -float('inf')
	for lab, val in dict1.items():
		curV = 0.0
		for w, v in fx.items():
			if w in weight[val]:
				curV += float(weight[val][w]) * float(v)
		if curV > maxV:
			maxV = curV
			predL = lab
	return predL	

def mainclassifier(dict1, w_avg, lines, total, classL):
	index = 0
	total = -1 
	correctL = 0
	for line in lines:
		fx = {}
		flag = True
		words = line.split(" ")
		if len(words) == 1:
			print()
		else:
			for word in words:
				if flag:
					flag = False
					label = word
				else:	
					if word not in fx:
						fx[word] = 1
					else:
						fx[word] += 1
			pred = prediction(dict1, w_avg, fx)
			if pred == label and pred != '':
				correctL += 1
			total += 1
			
	return pred	

def main():
	model = open(sys.argv[1], 'r', errors='ignore')

	dict1 = {}
	w_avg = [dict() for x in range(0)]
	mlines = map(lambda x:x.split(" "), model.readlines())
	index = 0
	lines = {}
	total1 = 0

	for mline in mlines:
		i = 0
		flag = True 
		for i in range(0, len(mline), 2):
			if i + 1 < len(mline) :
				word = mline[i]
				num = mline[i + 1]
				if flag:
					if word == 'SPACE':
						word = ''
					dict1[word] = index
					w_avg.append({})
					flag = False
					index += 1
					cur = dict1[word]
				else:
					w_avg[cur][word] =  int(num) 	
	sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')
	for line in sys.stdin:
		lines[total1] = line
		total1 += 1
	mainclassifier(dict1, w_avg, lines, total1, 3)

if __name__ == "__main__":
	main()
