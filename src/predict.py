#! /usr/bin/python

import nltk.data
from nltk.tag import pos_tag
import sys
import percepclassify

# Description : scan though the sentence, transform it into right format and call percepclassify
# function parameters:
# sents - sentence
# dict1, w_avg - parameters for percepclassify 
# return value:
# none 
def scanAndTransform(sents, dict1, w_avg):
	x = pos_tag(sents)
	inputDict = []
	for ind in range(0, len(sents)):	
		dindex = check(sents[ind])

		if dindex != -1:
			prev2L = x[ind - 2][1] if ind > 1 else " " 
			prevL = x[ind - 1][1] if ind > 0 else " " 
			nextL = x[ind + 1][1] if ind < (len(x) - 1) else " "	
			next2L = x[ind + 2][1] if ind < (len(x) - 2) else " "	
			prev2W = x[ind - 2][0] if ind > 1 else " " 
			prevW = x[ind - 1][0] if ind > 0 else " " 
			nextW = x[ind + 1][0] if ind < (len(x) - 1) else " "
			next2W = x[ind + 2][0] if ind < (len(x) - 2) else " "
			inputDict.append(x[ind][0] + " prevLabel::" + prevL + " nextLabel::" + nextL + " prevWord::" + prevW + " nextWord::" + nextW + " prev2Label::" + prev2L + " next2Label::" + next2L + " prev2Word::" + prev2W + " next2Word::" + next2W)  
			total = len(inputDict) 
			pred = percepclassify.mainclassifier(dict1[dindex], w_avg[dindex], inputDict, total, 0)
			pred = convert(pred)
			if pred != sents[ind]:
				sents[ind] = pred
			inputDict = []


# Description : print the transformed sentence 
# function parameters:
# sents - transformed sentence
# orig - orginal sentence
# out - output file handle
# return value:
# none 
def printout(sents, orig, out):
	for i in range(0, len(sents)):
		if orig[i].strip().lower() == sents[i].strip().lower():  
			out.write(orig[i] + " ")
		else:
			for j, ltr in enumerate(orig[i]):
				if ltr == '\n':
					out.write("\n")
				else:
					break

			if orig[i].isupper():
				out.write(sents[i].upper() + " ") 
			elif not orig[i].islower() and not orig[i].isupper():
				out.write(sents[i].title() + " ") 
			else:
				out.write(sents[i] + " ")   
			
				

# Description : main function which opens input, output files and calls all the necessary functions 
# function parameters:
# none 
# return value:
# none 
def main():
	inp = open(sys.argv[1], 'r')	
	out = open(sys.argv[3], 'w')
	dict1 = []
	dict1.append({}) 
	dict1.append({}) 
	dict1.append({}) 
	dict1.append({}) 
	dict1.append({}) 

	w_avg = []
	w_avg.append([dict() for x in range(0)])
	w_avg.append([dict() for x in range(0)])
	w_avg.append([dict() for x in range(0)])
	w_avg.append([dict() for x in range(0)])
	w_avg.append([dict() for x in range(0)])
	
	readmodels(dict1, w_avg)

	data = inp.read()
	flagP = False
	sents = []
	orig = []
	words = data.split(" ")
	for word in words:
		if check(word.strip()) != -1:
			flagP = True	
		orig.append(word)
		sents.append(word.strip())
		if '.' in word or '?' in  word or '!' in word:
			#print(sents)
			#print(orig)
			if flagP: 
				scanAndTransform(sents, dict1, w_avg)
				printout(sents, orig, out)
			else:
				for o in orig:
					out.write(o + " ")
			sents = []
			orig = []
	out.write('\n')  # check if this is necessary

	inp.close()
	out.close()


# Description : read model files 
# function parameters:
# dict1, w_avg - parameters for percepclassify 
# return value:
# none 
def readmodels(dict1, w_avg):
	for ii in range(0, 5):
		model = open(sys.argv[2] + "-" + str(ii), 'r')#, errors='ignore')

		mlines = map(lambda x:x.split(" "), model.readlines())
		index = 0

		for mline in mlines:
			i = 0
			flag = True 
			for i in range(0, len(mline), 2):
				if i + 1 < len(mline) :
					word = mline[i]
					num = mline[i + 1]
					#print(word)
					if flag:
						if word == 'SPACE':
							word = ''
						dict1[ii][word] = index
						w_avg[ii].append({})
						flag = False
						index += 1
						cur = dict1[ii][word]
					else:
						w_avg[ii][cur][word] =  int(num) 	


		model.close()

# Description : convert class label into word 
# function parameters:
# word - class label 
# return value:
# word
def convert(word):
	if word == 'it_s':
		return "it's"
	elif word == 'you_re':
		return "you're"
	elif word == 'they_re':
		return "they're"
	else: 
		return word
	
# Description : check  if the word is a relevant homonym 
# function parameters:
# sent - current word 
# return value:
# corresponding index 
def check(sent):
	if 'its' == sent.lower() or "it's" == sent.lower():
		return 0
	if 'your' == sent.lower() or "you're" == sent.lower():
		return 1 
	if 'their' == sent.lower() or "they're" == sent.lower():
		return 2 
	if 'lose' == sent.lower() or "loose" == sent.lower():
		return 3 
	if 'to' == sent.lower() or "too" == sent.lower():
		return 4 
	return -1

if __name__ == "__main__":
	main()
