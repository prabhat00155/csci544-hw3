#! /usr/bin/python

import sys
import math 
import re 
import random 


def prediction(dict1, weight, fx):
	maxV = -float('inf')
	for lab, val in dict1.items():
		curV = 0.0
		for w, v in fx.items():
			if w in weight[val]:
				curV += float(weight[val][w]) * float(v)
		if curV > maxV:
			maxV = curV
			predL = lab
	return predL	

def updateweights(dict1, weight, actualL, predL, fx):
	for w, v in fx.items():
		if w in weight[dict1[predL]]:
			weight[dict1[predL]][w] -= v
		if w in weight[dict1[actualL]]:
			weight[dict1[actualL]][w] += v

def addWord(dict1, weight, word):
	for lab, val in dict1.items():
		if word not in weight[val]:
			weight[val][word] = 0
		
def copyWeight(i, weight):
	for l, v in weight[0].items():
		weight[i][l] = 0 

def updateavg(dict1, weight, w_avg):
	for lab, val in dict1.items():
		for k, v in weight[val].items():
			if k not in w_avg[val]:
				w_avg[val][k] = 0
			w_avg[val][k] += v
	
def mainlearn(dict1, weight, lines, total):
	i = 0
	w_avg = [dict() for x in range(0)]
	prevError = 1.0
	curError = 0.99
	misCla = 0.0
	it = 0
	for x in range(0, 15): 
		it += 1
		misCla = 0.0
		for line in lines:
			#print(line)
			fx = {}
			flag = True 
			words = line.split(" ")
			for word in words:
				if flag:
					flag = False 
					label = word
					if label not in dict1:
						dict1[label] = i
						weight.append({})
						w_avg.append({})
						if i != 0:
							copyWeight(i, weight)	
						i += 1
					index = dict1[label]
				else:
					addWord(dict1, weight, word)
					if word in fx:
						fx[word] += 1
					else:
						fx[word] = 1
			pred = prediction(dict1, weight, fx)
			if pred != label:
				updateweights(dict1, weight, label, pred, fx)	
				misCla += 1
		prevError = curError
		curError = misCla / total
		print("Iteration =" , it, "Error = ", curError)
		random.shuffle(lines)

def main():
	inp = open(sys.argv[1], 'r', errors='ignore')
	model = open(sys.argv[2], 'w', errors='ignore')
	lines = map(lambda x:x.split(" "), inp.readlines())
	inputDict = []
	total = 0
	for line in lines:
		flag = True
		for word in line:
			if flag:
				inputDict.append(word + ' ')
				flag = False
			else:
				inputDict[total] += word + ' '
		total += 1
	dict1 = {}
	w_avg = [dict() for x in range(0)]
			
	mainlearn(dict1, w_avg, inputDict, total)
	for kd, vd in dict1.items():
		if kd == '' or kd == ' ':
			kd = 'SPACE'
		model.write(kd + ' ' + str(vd) + ' ')
		for kw, vw in w_avg[vd].items():
			kw = kw.replace('\n', '')
			if kw != '' and kw != ' ':
				model.write(kw + ' ' + str(vw) + ' ') 
		model.write('\n')
	inp.close()
	model.close()

if __name__ == "__main__":
	main()
